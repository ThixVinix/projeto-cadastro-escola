var inputCep = document.querySelector("#idCep");

inputCep.addEventListener("blur", function (event) {
    event.preventDefault();

    buscarEndereco(this.value);

});


//Exemplo de requisição HTTP
function buscarEndereco(cep) {


    if (cep.length == 8) {
        var xhr = new XMLHttpRequest()

        xhr.open("GET", "https://viacep.com.br/ws/" + cep + "/json/")

        xhr.addEventListener("load", function () {
            var resposta = xhr.responseText
            var respostaOBJ = JSON.parse(resposta)


            if (!isNaN(Number(respostaOBJ.ddd))) {
                document.getElementById("idInputRua").value = respostaOBJ.logradouro
                document.getElementById("idInputBairro").value = respostaOBJ.bairro
                document.getElementById("idInputCidade").value = respostaOBJ.localidade
                document.getElementById("idUf").value = respostaOBJ.uf
                console.log(respostaOBJ);
            } else {
                document.getElementById("idInputRua").value = ""
                document.getElementById("idInputBairro").value = ""
                document.getElementById("idInputCidade").value = ""
                document.getElementById("idUf").value = ""
            }


        })

        xhr.send()
    } else {
        document.getElementById("idInputRua").value = ""
        document.getElementById("idInputBairro").value = ""
        document.getElementById("idInputCidade").value = ""
        document.getElementById("idUf").value = ""
    }

}